import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StockofficineService {

  constructor(private http: HttpClient) { }

  getStockofficines() {
    return this.http.get('http://79583990.ngrok.io/stockofficine');
  }

  getStockofficine(id) {
    return this.http.get('http://79583990.ngrok.io/stockofficine' + id);
  }
}
