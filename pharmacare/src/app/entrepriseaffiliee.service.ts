import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EntrepriseaffilieeService {

  constructor(private http: HttpClient) { }

  getEntrepriseaffiliees() {
    return this.http.get('http://79583990.ngrok.io/entrepriseaffiliee');
  }

  getEntrepriseaffiliee(id) {
    return this.http.get('http://79583990.ngrok.io/entrepriseaffiliee' + id);
  }
}
