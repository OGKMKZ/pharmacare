import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StockmagasinService {

  constructor(private http: HttpClient) { }

  getAssureurs() {
    return this.http.get('http://79583990.ngrok.io/assureur');
  }

  getAssureur(id) {
    return this.http.get('http://79583990.ngrok.io/assureur' + id);
  }
}
