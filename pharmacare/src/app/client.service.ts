import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }

  getClients() {
    return this.http.get('http://79583990.ngrok.io/client');
  }

  getClient(id) {
    return this.http.get('http://79583990.ngrok.io/client' + id);
  }

  addClient() {

  }
}
