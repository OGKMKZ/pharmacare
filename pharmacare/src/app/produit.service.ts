import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {


  constructor(private http: HttpClient) { }

  getProduits() {
    return this.http.get('http://79583990.ngrok.io/product');
  }

  getProduit(id) {
    return this.http.get('http://79583990.ngrok.io/product' + id);
  }
}
