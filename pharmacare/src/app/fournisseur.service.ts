import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FournisseurService {

  constructor(private http: HttpClient) { }

  getFournisseurs() {
    return this.http.get('http://79583990.ngrok.io/fournisseur');
  }

  getFournisseur(id) {
    return this.http.get('http://79583990.ngrok.io/fournisseur' + id);
  }
}
